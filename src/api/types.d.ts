// Widgets
export interface WidgetColorSchema {
  primaryText: string;
  secondaryText: string;
  primaryUi: string;
  primaryConversation: string;
  secondaryConversation: string;
  primaryAccent: string;
  secondaryAccent: string;
}

export interface WidgetCaptions {
  welcome: string;
  inputTooltip: string;
}

export interface Widget {
  widgetId: string;
  apiKey: string;
  widgetName: string;
  colorSchema: WidgetColorSchema;
  captions: WidgetCaptions;
}

export interface NewWidget {
  widgetId: string;
  widgetName: string;
  colorSchema: WidgetColorSchema;
  captions: WidgetCaptions;
}

// Flows

export interface Flow {
  flowId: string;
  started: string;
}

// Conversation

export interface Conversation {
  timestamp: string;
  question: string | null;
  answer: string;
}
