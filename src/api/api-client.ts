'use client';

import axios, { AxiosResponse } from 'axios';

import { useMutation, useQuery } from 'react-query';
import queryClient from '@api/query-client';
import { InputHTMLAttributes, useCallback, useRef, useState } from 'react';
import { cloudConfig } from './cloudinary';
import { CDYPresets, MediaType } from 'src/types/media.type';

export const api = axios.create({
  baseURL:
    process.env.NODE_ENV == 'production'
      ? 'https://mmry-api-production.up.railway.app/api/v1'
      : 'http://localhost:4000/api/v1',
});

export const useGetCardType = (cardId: string) => {
  return useQuery(
    'getCardType',
    (): Promise<AxiosResponse<{ type: 'UPLOAD' | 'PLAY' }>> =>
      api.get(`/memories-card/type/${cardId}`),
    { select: (result) => result.data }
  );
};

export const useCardPlay = () => {
  // return useQuery(
  //   'getCardPlay',
  //   (): Promise<AxiosResponse<{ data: any }>> => api.get(`/card/play/${cardId}`)
  // );2222
  return useMutation({
    mutationFn: async ({ id, password }: { id: string; password: number }) => {
      const { data, status } = await api.post('/memories-card/play', {
        id,
        password,
      });
      return data;
    },
    onError: (error: any) => {
      if (error.code == 'ERR_NETWORK') {
        error.message = 'Sorry, we have a problem with a server';
      }
      if (error?.response?.status === 405) {
        error.message = 'Wrong password!';
      }
    },
  });
};
export const useCreateCard = () => {
  return useMutation({
    mutationFn: async ({ id, password }: { id: string; password: string }) => {
      const { data, status } = await api.post('/memory/create', {
        id,
        password,
      });
      return data;
    },
    onSuccess: (data: any) => {
      return data.status;
    },
    onError: (error: any) => {
      console.log({ error });
      if (error.code == 'ERR_NETWORK') {
        error.message = 'Sorry, we have a problem with a server';
      }
      if (error?.response?.status === 405) {
        // Handle 401 error here
        error.message = 'Card with this ID already exists';
      }
    },
  });
};
export const useUploadCard = () => {
  const [progress, setProgress] = useState(0);
  const mutation = useMutation({
    mutationFn: async ({ id, videoURL }: { id: string; videoURL: string }) => {
      const assets = [{ type: 'VIDEO', fileName: 'default', uri: videoURL }];
      const activatedAt = new Date().getTime();
      const { data } = await api.put(
        '/memories-card',
        { id, assets, activatedAt },
        {
          onUploadProgress: (event: any) => {
            setProgress(Math.round((100 * event.loaded) / event?.total ?? 0));
          },
        }
      );
      console.log({ data });
      return data;
    },
    onError: (error: any) => {
      if (error?.response?.status === 405) {
        // Handle 401 error here
        error.message = 'Video already added';
      }
    },
  });
  return { ...mutation, progress };
};
export interface Video {
  url: string;
  name: string;
  size: number;
  image?: string;
}

const deleteVideoFromCloudinary = async (videoId: string) => {
  return await api.delete(`/media/delete/${videoId}`);
};

export const useDeleteVideo = () => {
  return useMutation({
    mutationFn: async ({ videoId }: { videoId: string }) => {
      if (!videoId) {
        return [];
      }

      const idFromURL = videoId.split('/videos/')[1]?.split('.')[0];
      if (!idFromURL) {
        return [];
      }
      const result = deleteVideoFromCloudinary(idFromURL);
      return result;
    },

    onError: (error: any) => {
      if (error) {
        // Handle 401 error here
        error.message = 'Video is not deleted';
      }
    },
  });
};

const defaultVideo = {
  url: '',
  name: '',
  size: 0,
};
interface UploadFileToCDYProps {
  file: File;
  preset: CDYPresets;
  resourceType: MediaType;
}
export const useUploadFileToCDY = () => {
  const [progress, setProgress] = useState(0);
  const [thumbnailVideoURL, setThumbnailVideoURL] = useState('');
  const abortControllerRef = useRef<AbortController | null>(null);
  const [video, setVideo] = useState<Video>(defaultVideo);
  const setDefaultValues = () => {
    setVideo(defaultVideo);
  };
  const mutation = useMutation(['upload-file'], {
    mutationFn: async ({
      file,
      preset,
      resourceType,
    }: UploadFileToCDYProps) => {
      abortControllerRef.current = new AbortController();
      const videoSizeMB = Number((video.size / 1000024).toFixed());
      setVideo(
        (state): Video => ({ ...state, size: videoSizeMB, name: video.name })
      );

      if (videoSizeMB > 100) {
        return [];
      }

      const data = new FormData();
      data.append('file', file);
      data.append('upload_preset', preset);
      try {
        const cloundName = process.env.NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME;
        const api = `https://api.cloudinary.com/v1_1/${cloundName}/${resourceType}/upload`;
        const res = await axios.post(api, data, {
          onUploadProgress: (event: any) => {
            setProgress(Math.round((100 * event.loaded) / event?.total ?? 0));
          },
          signal: abortControllerRef.current.signal,
        });
        const { secure_url } = res.data;
        if (secure_url) {
          setVideo((state): Video => ({ ...state, url: secure_url }));
          return {
            url: secure_url,
            name: video.name,
            size: videoSizeMB,
          };
        }
      } catch (error) {
        console.log(error);
      }
      return null;
    },
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: 'card' });
    },
    onError: (error: any) => {
      if (error?.response?.status === 405) {
        // Handle 401 error here
        error.message = 'Video already added';
      }
    },
  });
  const abort = useCallback(() => {
    abortControllerRef.current?.abort();
    mutation.reset();
    setDefaultValues();
    setProgress(0);
  }, [mutation.reset, setProgress]);
  return {
    ...mutation,
    progress,
    data: video,
    setDefaultValues,
    abort,
    thumbnail: thumbnailVideoURL,
  };
};

export const useCreateCards = () => {
  return useMutation({
    mutationFn: async (quantity: number) => {
      console.log({ quantity });
      const { data, status } = await api.post('/memories-card/collection', {
        quantity: quantity,
      });
      return data;
    },
    onError: (error: any) => {
      console.log('ERROR', error);
      if (error.code == 'ERR_NETWORK') {
        error.message = 'Sorry, we have a problem with a server';
      }
      if (error?.response?.status === 405) {
        // Handle 401 error here
        error.message = 'Card with this ID already exists';
      }
    },
  });
};
