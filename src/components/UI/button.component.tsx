// import { ButtonProps, Button as NextButton } from '@nextui-org/react';

// export const Button = ({ variant, className, ...props }: ButtonProps) => {
//   return (
//     <NextButton
//       className={`${
//         variant == 'bordered' && 'border-1'
//       } ${
//         variant == 'solid' && 'bg-gradient-to-r from-primary-400 to-primary-600 text-shadow-50 font-bold'
//       }  ${className}`}
//       //   color="primary"
//       size={'lg'}
//       variant={variant}
//       {...props}
//     ></NextButton>
//   );
// };

import { Button as NextButton, ButtonProps as NextButtonProps } from '@nextui-org/react'
import { cva } from 'class-variance-authority'

import { LucideIcon } from 'lucide-react'
import * as React from 'react'
import { cn } from 'src/lib/utils'

const buttonVariants = cva(
  'inline-flex gap-2 items-center justify-center rounded-md font-bold ring-offset-background transition  disabled:pointer-events-none disabled:opacity-50  text-3xl',
  {
    variants: {
      variant: {
        default:
          'text-primary-foreground hover:bg-primary/90 hover:shadow-lg hover:shadow-primary/20 bg-gradient-to-r from-primary-400 to-primary-600',
        ghost: 'hover:bg-accent hover:text-accent-foreground',
        light: 'hover:bg-primary-50 bg-transparent text-primary',
        shadow: '',
        flat: '',
        danger: 'bg-gradient-to-r from-danger-400 to-danger-600 text-shadow-50 font-bold',
        solid: 'bg-gradient-to-r from-primary-400 to-primary-600 text-shadow-50 font-bold',
        bordered:
          'outline-primary-500 outline-1 text-primary bg-transparent hover:text-accent-foreground outline-offset-[-1px]',
        faded: '',
      },
      size: {
        default: 'min-h-10 px-4 py-2 text-sm',
        sm: 'min-h-9 rounded-md px-3 py-1.5 text-sm',
        md: 'min-h-9 rounded- px-3 py-1.5 text-sm',
        lg: 'min-h-11 rounded-xl px-7 py-6 text-md',
      },
    },
    defaultVariants: {
      variant: 'default',
      size: 'default',
    },
  }
)

export interface ButtonProps extends NextButtonProps {
  variant?: any
  asChild?: boolean
  icon?: LucideIcon 
  color?: 'default' | 'secondary' | 'primary' | 'success' | 'warning' | 'danger' | undefined
  iconPosition?: 'start' | 'end'
  label?: string
  bouncyIcon?: boolean
  customIcon?: React.ReactNode,
}

const Button = ({
  className,
  variant = 'solid',
  size = 'lg',
  icon: Icon,
  customIcon,
  iconPosition = 'start',
  label,
  onPress,
  ...props
}: ButtonProps) => {
  return (
    <NextButton
    onPress={onPress}
      className={cn(buttonVariants({ variant, size, className }), {
        'text-left': Icon,
      })}
      size={size}
      // ref={ref}
      {...props}
    >
      {Icon && iconPosition === 'start' && <Icon size={size === 'lg' ? 24 : 20} />}
      {customIcon && iconPosition === 'start' && customIcon}
      {props.children && (
        <div className="flex flex-col gap-0.5 text-md">
          {props.children}
          {label && (
            <span className="text-semibold self-start bg-green-100 px-1 py-0.5 text-lg uppercase text-green-500">
              {label}
            </span>
          )}
        </div>
      )}
      {Icon && iconPosition === 'end' && (
        <Icon
          size={size === 'lg' ? 24 : 20}
        />
      )}
    </NextButton>
  )
}
Button.displayName = 'Button'

export { Button, buttonVariants }
