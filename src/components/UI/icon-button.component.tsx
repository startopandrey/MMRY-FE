import React, { ReactNode } from 'react';
import { Button, ButtonProps } from '@nextui-org/button';
import { cva } from 'class-variance-authority';
import { cn } from 'src/lib/utils';
const iconButtonVariants = cva(
  'min-w-0 rounded-[50%] relative',
  {
    variants: {
      size: {
        default: 'h-[50px] w-[50px]',
        sm: 'h-[32px] w-[32px]',
        md: 'h-[40px] w-[40px]',
        lg: 'h-[50px] w-[50px]',
      },
    },
  }
);
interface IconButton extends ButtonProps {
  icon: ReactNode;
}
export const IconButton = ({ className, icon, size, ...props }: IconButton) => {
  return (
    <Button
      className={cn(iconButtonVariants({ size, className }))}
      size={size}
      color={'secondary'}
      {...props}
    >
      <div className="abolute">{icon}</div>
    </Button>
  );
};
