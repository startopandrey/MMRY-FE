import React, {
  HTMLProps,
  LegacyRef,
  RefObject,
  useCallback,
  useRef,
} from 'react';
import { Button } from './button.component';
import { useUploadFileToCDY } from '@api/api-client';
import { CDYPresets, MediaType } from 'src/types/media.type';
import { getFileType } from '@helpers/media.helper';

interface FileButtonProps {
  onChange: (file: FileDataButton) => void;
  accept: React.HTMLInputTypeAttribute;
}
export interface FileDataButton {
  data: File;
  type: MediaType;
  id: number;
  url?: string;
  fileName?: string;
}

const FileButton = ({
  accept = 'file',
  onChange,
  ...props
}: FileButtonProps) => {
  const ref = useRef<HTMLInputElement>(null);
  const handleClick = () => {
    // `current` points to the mounted file input element
    if (ref?.current) {
      // @ts-ignore
      ref?.current?.click();
    }
  };
  const handleInputChangeVideo = async (e: any) => {
    e.preventDefault();
    // handleFileChange(e, setThumbnailVideoURL);
    const file = e.target.files[0];
    if (file) {
      const fileType = getFileType(file);
      if (fileType == 'image' || fileType == 'video') {
        onChange({
          data: file,
          fileName: file?.name,
          id: Date.now(),
          type: fileType,
        });
      }
    }
  };
  return (
    <div>
      <input
        ref={ref}
        type="file"
        id="file"
        style={{ display: 'none' }}
        onChange={handleInputChangeVideo}
        {...props}
      />
      <Button variant="solid" className="mb-2" onClick={handleClick}>
        browse
      </Button>
    </div>
  );
};

export default FileButton;
