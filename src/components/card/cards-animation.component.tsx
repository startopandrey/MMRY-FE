import React from 'react';
import { motion } from 'framer-motion';
interface CardsAnimation {
    index: number, children: React.ReactNode
}
export const CardsAnimation = ({ index, children }: CardsAnimation) => {
  return (
    <motion.div
      transition={{
        type: 'spring',
        opacity: { duration: 0.1 },
      }}
      animate={{ translateX: `-${index * 100}%` }}
      initial="center"
      className="flex items-center gap-[16px] mr-[-16px]"
    >
      {children}
    </motion.div>
  );
};
