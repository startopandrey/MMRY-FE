import React, { useCallback, useEffect, useRef, useState } from 'react';
import { CldVideoPlayer } from 'next-cloudinary';
import 'next-cloudinary/dist/cld-video-player.css';

import { useCardPlay } from '@api/api-client';
import {
  Card,
  CardBody,
  CardHeader,
  Modal,
  ModalContent,
} from '@nextui-org/react';
import OtpInput from 'react-otp-input';
import { IconButton } from '@components/UI/icon-button.component';
import { CloudFog, Play, X } from 'lucide-react';
import Image from 'next/image';
import {
  TextWithLink,
  cardPlayAdvertise,
} from 'src/data/card-play/card-play.data';
import { Button } from '@components/UI/button.component';
import { PlayIcon } from '@icons/play.icon';
import { CardsAnimation } from '../cards-animation.component';
import { StepsProgress } from '@components/steps-progress/steps-progress.component';

interface CardPlayProps {
  cardId: string;
}

const ParagraphWithMarked = ({ data }: { data: TextWithLink[] }) => {
  console.log(data, 'df');
  return (
    <p key={data[0]?.text} className="mb-2 w-full text-center text-sm">
      {data.map((row: TextWithLink) =>
        row.text == '3 maanden' ? (
          <span key={row.text} className="text-danger ">
            {row.text}
          </span>
        ) : row.href ? (
          <a key={row.text} href={row.href} className="text-primary underline">
            {row.text}
          </a>
        ) : (
          <span key={row.text} className=" text-shadow-600">
            {' '}
            {row.text}
          </span>
        )
      )}
    </p>
  );
};
export const CardPlay = ({ cardId }: CardPlayProps) => {
  const playerRef = useRef(null);
  const [password, setPassword] = useState('');
  const [selectedCardIndex, setSelectedCardIndex] = useState(0);
  const advertiseDataRows = cardPlayAdvertise.sections[0]?.listWithLinks;

  const {
    mutate: mutateCardPlay,
    data: cardPlay,
    isLoading: isLoadingCardPlay,
    error: errorCardPlay,
  } = useCardPlay();
  const getCard = useCallback(
    (cardId: string, password: number) =>
      mutateCardPlay(
        { id: cardId, password },
        { onSuccess: () => setSelectedCardIndex(1) }
      ),
    [mutateCardPlay]
  );

  const handleReplay = () => {
    setSelectedCardIndex(1);
  };
  const handleStepNext = () => {
    if (selectedCardIndex == 0) {
      getCard(cardId, +password);
      return;
    }
    setSelectedCardIndex((state) => state + 1);
  };
  const handleStepBack = () => {
    setSelectedCardIndex(0);
  };
  const handleSubmitPassword = ()=> {
    getCard(cardId, +password);
  }
  const onVideoEnded = () => {
    setSelectedCardIndex(2);
  };
console.log({cardPlay})
  return (
    <div className="mt-4 w-full container overflow-x-hidden">
      {selectedCardIndex < 2 ? (
        <>
          <CardsAnimation index={selectedCardIndex}>
            <div>
              <Card className=" w-[calc(100vw-32px)] max-h-[100vh-400px] border-0 p-6 my-3 shadow-none  rounded-3xl">
                <CardHeader className="flex flex-col justify-center items-center">
                  {' '}
                  <h2 className="text-3xl font-bold text-center text-shadow-800 my-2">
                    Wachtwoord invoeren
                  </h2>
                  <p className="text-sm font-medium w-8/12 text-center text-shadow-400">
                    *De 6 cijfers onder je QR-code
                  </p>
                  {errorCardPlay && (
                    <p className="text-md font-semibold mt-1 w-8/12 text-center text-danger-400">
                      {errorCardPlay.message}
                    </p>
                  )}
                </CardHeader>
                <CardBody className=" w-full  relative  overflow-y-hidden">
                  <OtpInput
                    value={password}
                    onChange={setPassword}
                    numInputs={6}
                    containerStyle={'w-full flex justify-between gap-2'}
                    inputType="number"
                    inputStyle={
                      'focus:outline-neutral-400 outline-neutral-200 outline-neutral-200 outline-1 outline rounded-md !w-10 h-10 text-primary-900 text-lg text-center'
                    }
                    renderInput={(props) => <input {...props} />}
                  />
                  <Button
                    className="w-full text-white mt-8"
                    variant="solid"
                    isDisabled={selectedCardIndex == 0 && password.length < 6}
                    isLoading={isLoadingCardPlay}
                    onClick={handleSubmitPassword}
                  >
                    Video bekijken
                  </Button>
                </CardBody>
              </Card>
            </div>
            <div>
              <Card className=" w-[calc(100vw-32px)]  max-h-[100vh-400px] border-0 p-2 my-3 shadow-none  rounded-3xl">
                <CardBody className=" w-auto h-[400px] relative  overflow-y-hidden card-player-video-container">
                  <div className="w-auto h-[400px]">
                    {cardPlay?.assets[0].uri ? (
                      <CldVideoPlayer
                        playerRef={playerRef}
                        className={'text-center h-[400px]'}
                        onEnded={onVideoEnded}
                        controls
                        autoPlay={true}
                        width={'100%'}
                        height={'100%'}
                        logo={false}
                        src={cardPlay?.assets[0].uri}
                        colors={{
                          base: '#3ba5de',
                        }}
                      />
                    ) : null}
                  </div>
                </CardBody>
              </Card>
            </div>
          </CardsAnimation>

          {/* <StepsProgress
            selectedIndex={selectedCardIndex}
            isFinished={selectedCardIndex < 2}
            progress={53 * (selectedCardIndex + 1)}
            onClickBack={handleStepBack}
            onClickNext={handleStepNext}
            isDisable={selectedCardIndex == 0 && password.length < 6}
            isLoading={isLoadingCardPlay}
          ></StepsProgress> */}
        </>
      ) : (
        <div>
          <div className="flex flex-col items-center justify-center gap-4 w-[calc(100vw-32px)]">
            <Card className="  bg-white w-auto py-6 outline-none shadow-none">
              <CardHeader className="flex justify-center">
                <Image
                  alt={'Logo'}
                  width={100}
                  height={50}
                  src="/black-logo.png"
                ></Image>
              </CardHeader>
              <CardBody>
                <div>
                  {advertiseDataRows?.map((row: any) => (
                    <ParagraphWithMarked
                      key={row.text}
                      data={row}
                    ></ParagraphWithMarked>
                  ))}
                </div>
                <Button
                  customIcon={<PlayIcon></PlayIcon>}
                  className="w-full my-3"
                  variant="bordered"
                  onClick={handleReplay}
                >
                  Opnieuw bekijken
                </Button>
                <div className="flex gap-4 justify-center mt-3">
                  <div className="">
                    <Image
                      className="w-full h-full object-contain"
                      objectFit="full"
                      width={200}
                      alt={'apple store'}
                      height={100}
                      src="/icons/apple-store.png"
                    ></Image>
                  </div>
                  <div className="">
                    <Image
                      className="w-full h-full object-contain"
                      width={200}
                      alt={'apple store'}
                      height={100}
                      src="/icons/google-store.png"
                    ></Image>
                  </div>
                </div>
              </CardBody>
            </Card>
          </div>
        </div>
      )}

      {/* <Modal
          classNames={{ backdrop: ' bg-transparent z-50', body: 'py-0 ' }}
          isOpen={isVideoShown}
          placement="top-center"
          onClose={toggleVideoModal}
          hideCloseButton={true}
        >
          <ModalContent
            className={'h-[10vh-60px] shadow-none mx-4 bg-transparent'}
          >
            {(onClose) => (
       
            )}
          </ModalContent>
        </Modal> */}
    </div>
  );
};
