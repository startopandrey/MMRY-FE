'use client';

import {
  Card,
  CardBody,
  CardHeader,
  Divider,
  Modal,
  ModalContent,
  ModalFooter,
  ModalHeader,
  Spinner,
} from '@nextui-org/react';
//@ts-ignore
import Image from 'next/image';
import React, { useCallback, useRef, useState } from 'react';
import 'next-cloudinary/dist/cld-video-player.css';
import { Button } from 'src/components/UI/button.component';
import { CancelIcon } from 'src/icons/cancel.icon';
import { UploadIcon } from 'src/icons/upload.icon';
import Lottie from 'lottie-react';
import successAnimation from '../../../../public/animations/success.json';
import {
  useDeleteVideo,
  useUploadCard,
  useUploadFileToCDY,
} from '@api/api-client';
import { useRouter } from 'next/navigation';
import { StepsProgress } from '@components/steps-progress/steps-progress.component';
import { VideoPlayer } from '@components/video-player.component';
import { useQueryClient } from 'react-query';
import { generateVideoThumbnails } from '@rajesh896/video-thumbnails-generator';
import { CardsAnimation } from '../cards-animation.component';
interface CardUploadProps {
  cardId: string;
}

const handleFileChange = async (
  e: any,
  setThumbnailUrl: (url: string) => void
) => {
  const file = e.target.files[0];
  // Check if the selected file is a video
  const thumbnail: string[] = await generateVideoThumbnails(file, 1, 'base64');
  if (thumbnail[0]) {
    setThumbnailUrl(thumbnail[0]);
  }
};

export const CardUpload = ({ cardId }: CardUploadProps) => {
  const router = useRouter();
  const queryClient = useQueryClient();
  //hooks

  const {
    mutate: mutateUploadVideo,
    isLoading: isLoadingUploadVideo,
    error: errorUploadVideo,
    progress: videoUploadProgress,
    data: { size: videoSize, url: videoURL, name: videoName },
    setDefaultValues: setVideoToDefaultValues,
    reset: resetUploadVideo,
    abort: abortUploadVideo,
  } = useUploadFileToCDY();

  const {
    mutate: mutateDeleteVideo,
    isSuccess: isSuccessDeleteVideo,
    isLoading: isLoadingDeleteVideo,
  } = useDeleteVideo();
  console.log(videoURL, 'vidoe');
  // .split("/videos/")[1].split(".")
  const {
    mutate: mutateUploadCard,
    isSuccess: isSuccessUploadCard,
    isLoading: isLoadingUploadCard,
    error,
    progress: progressUploadCard,
  } = useUploadCard();

  //states
  const [confirmationModalToggle, setConfirmationModalToggle] = useState({
    isOpen: false,
    isActive: false,
  });
  const [thumbnailVideoURL, setThumbnailVideoURL] = useState('');
  const [selectedCardIndex, setSelectedCardIndex] = useState(0);
  console.log(selectedCardIndex);

  const videoFileRef = useRef<HTMLInputElement | null>(null);

  // const [videoURL, setVideoURL] = useState(null);

  const uploadCard = useCallback(
    (cardId: string, videoURL: string) =>
      mutateUploadCard(
        { id: cardId, videoURL: videoURL },
        { onSuccess: () => setSelectedCardIndex(3) }
      ),
    [mutateUploadCard, videoURL]
  );
  const deleteVideoFromCloud = useCallback(
    (videoId: string) => mutateDeleteVideo({ videoId }),
    [mutateDeleteVideo]
  );
  const uploadVideo = useCallback(
    (video: File) =>
      mutateUploadVideo({
        file: video,
        preset: 'memories_videos',
        resourceType: 'video',
      }),
    [mutateUploadVideo]
  );

  const handleStepNext = () => {
    if (selectedCardIndex == 1) {
      setConfirmationModalToggle({ isOpen: true, isActive: true });
      return;
    }
    setSelectedCardIndex((state) => state + 1);
  };
  const handleStepBack = () => {
    setSelectedCardIndex(0);
  };
  const handleClickUploadCard = () => {
    setConfirmationModalToggle({ isActive: true, isOpen: false });
    setSelectedCardIndex((state) => state + 1);
    uploadCard(cardId, videoURL);
    setSelectedCardIndex(3);
  };

  const handleClickUpload = () => {
    // `current` points to the mounted file input element
    if (videoFileRef?.current) {
      videoFileRef?.current?.click();
    }
  };

  const handleInputChangeVideo = async (e: any) => {
    e.preventDefault();
    handleFileChange(e, setThumbnailVideoURL);
    const videoFile = e.target.files[0];
    if (videoFile) {
      uploadVideo(videoFile);
    }
  };
  const changeVideo = async () => {
    if (!videoFileRef.current) {
      return;
    }
    if (videoURL) {
      deleteVideoFromCloud(videoURL);
    }
    abortUploadVideo();
    resetUploadVideo();
    videoFileRef.current.value = '';
    setVideoToDefaultValues();
    setSelectedCardIndex(0);
    setConfirmationModalToggle({ isOpen: false, isActive: false });
  };
  console.log(thumbnailVideoURL, 'fd');
  return (
    <div className="mt-4 w-full container overflow-x-hidden card-upload-container">
      <CardsAnimation index={selectedCardIndex}>
        <div>
          <Card className=" w-[calc(100vw-32px)] border-0 p-2 my-3 shadow-md shadow-primary-50 rounded-3xl">
            <CardHeader>
              {' '}
              <h3 className="text-md font-bold text-center text-shadow-600">
                Selecteer een video op je telefoon
              </h3>
            </CardHeader>
            <CardBody>
              {videoFileRef?.current?.files?.length ? (
                <div
                  className={
                    'relative bg-primary-50 p-2  rounded-2xl flex justify-between items-center overflowx-x-hidden '
                  }
                >
                  <div
                    style={{ width: `${100 * (videoUploadProgress / 100)}%` }}
                    className="absolute  top-0 left-0 rounded-2xl bg-primary-100/50 h-full w-3 transition-width duration-200"
                  ></div>
                  <div className="flex items-center z-10">
                    <div className="w-16 h-16  rounded-xl overflow-hidden flex items-center border-primary-300 border-3">
                      {thumbnailVideoURL[0] && (
                        <Image
                          className="w-full h-full object-cover "
                          width={100}
                          height={100}
                          alt="none"
                          objectFit="cover"
                          src={thumbnailVideoURL}
                        ></Image>
                      )}
                    </div>
                    <div className="ml-3">
                      <h3 className=" text-shadow-800 font-bold overflow-hidden truncate w-[120px]">
                        {videoName}
                      </h3>
                      <p className=" text-xs text-shadow-500 z-50">
                        {(videoSize * (videoUploadProgress / 100)).toFixed(2)}{' '}
                        MB of {videoSize} MB
                      </p>
                    </div>
                  </div>{' '}
                  <div className="mr-3 z-50">
                    <h3 className=" font-semibold  text-primary-600">
                      {videoUploadProgress}%
                    </h3>
                  </div>
                  <div
                    className="mr-3 cursor-pointer z-50 w-4"
                    onClick={changeVideo}
                  >
                    <CancelIcon></CancelIcon>
                  </div>
                </div>
              ) : (
                <div className="flex justify-center">
                  <Image
                    alt={'library not found'}
                    src={'/library-not-found.png'}
                    objectFit="contain"
                    width={220}
                    height={100}
                  ></Image>
                </div>
              )}

              <Divider className="my-4"></Divider>
              <input
                ref={videoFileRef}
                type="file"
                id="file"
                accept="video/*"
                style={{ display: 'none' }}
                onChange={handleInputChangeVideo}
              />

              <Button
                variant="bordered"
                color={'primary'}
                className="border-1 h-12"
                isDisabled={!!videoURL || isLoadingUploadVideo}
                startContent={<UploadIcon></UploadIcon>}
                onClick={handleClickUpload}
              >
                {' '}
                <h3 className="text-base font-bold">Uploaden</h3>{' '}
              </Button>
              <p className="text-sm text-shadow-400 mt-2">
                Video limit is <span className=" text-danger-400">100MB</span>{' '}
              </p>
            </CardBody>
          </Card>
        </div>
        <div>
          <Card className="w-[calc(100vw-32px)]  border-0 p-2 my-3 shadow-md shadow-primary-50 rounded-3xl">
            <CardHeader className="flex flex-col justify-center items-center">
              {' '}
              <h2 className="text-3xl font-bold text-center text-shadow-800 my-2">
                Check je video
              </h2>
              <p className="text-sm font-medium w-8/12 text-center text-shadow-400">
                Controleer of je de juiste video hebt gekozen.
              </p>
            </CardHeader>
            <CardBody>
              <div className="w-[100%] max-h-[50%]  h-[150px] rounded-lg  relative mb-2">
                <VideoPlayer url={videoURL}></VideoPlayer>
              </div>
              <Button variant="light" onClick={changeVideo}>
                Nee, ik wil een andere video
              </Button>
            </CardBody>
          </Card>
          <Modal
            isOpen={confirmationModalToggle.isOpen}
            placement="center"
            onOpenChange={(state) =>
              setConfirmationModalToggle((prevState) => ({
                ...prevState,
                isOpen: state,
              }))
            }
            scrollBehavior={'outside'}
            hideCloseButton={true}
            className=" rounded-[32px] p-4 mx-4 translate-y-[-60px]"
          >
            <ModalContent>
              {(onClose) => (
                <>
                  <ModalHeader className="flex flex-col items-center justify-center gap-1">
                    <h2 className="text-4xl text-center font-bold text-shadow-800">
                      Weet je het zeker?
                    </h2>
                    <p className="text-center text-shadow-400 my-1">
                      Na deze bevestiging kun je de video niet meer wijzigen.
                    </p>
                  </ModalHeader>
                  <ModalFooter className="flex flex-col">
                    <Button color="primary" onPress={handleClickUploadCard}>
                      Ja, zeker weten
                    </Button>
                    <Button
                      color="danger"
                      variant="light"
                      onPress={changeVideo}
                    >
                      Nee, ik wil een andere video
                    </Button>
                  </ModalFooter>
                </>
              )}
            </ModalContent>
          </Modal>
        </div>
        <div>
          <Card className="w-[calc(100vw-32px)]  border-0 p-2 my-3 shadow-md shadow-primary-50 rounded-3xl">
            <CardHeader className="flex flex-col justify-center items-center">
              {' '}
              <h2 className="text-4xl font-bold text-center text-shadow-800 my-2">
                Uploading a video...
              </h2>
              <p className="text-md font-medium w-8/12 text-center text-shadow-400">
                Please, don&apos;t close the window while loading.
              </p>
            </CardHeader>
            <CardBody className="flex justify-center items-center h-40">
              <div className="flex justify-center scale-150">
                <Spinner size="lg"></Spinner>
              </div>
            </CardBody>
          </Card>
        </div>
        <div>
          <Card className="w-[calc(100vw-32px)]  border-0 p-2 my-3 shadow-md shadow-primary-50 rounded-3xl">
            <CardHeader className="flex flex-col justify-center items-center">
              {' '}
              <h2 className="text-3xl font-bold text-center text-shadow-800 my-2">
                Gefeliciteerd!!!
              </h2>
              <p className="text-sm font-medium text-center text-shadow-400">
                Je video staat nu op je MMRY kaart! Je kunt de QR-code nu
                scannen om de video af te spelen.
              </p>
            </CardHeader>
            <CardBody className="flex justify-center items-center h-52">
              <div className="flex justify-center w-[180px] ">
                {selectedCardIndex == 3 ? (
                  <Lottie
                    animationData={successAnimation}
                    loop={false}
                  ></Lottie>
                ) : null}
              </div>
            </CardBody>
          </Card>
        </div>
      </CardsAnimation>
      {selectedCardIndex < 2 ? (
        <StepsProgress
          selectedIndex={selectedCardIndex}
          isFinished={selectedCardIndex < 2}
          progress={40 * (selectedCardIndex + 1)}
          onClickBack={handleStepBack}
          onClickNext={handleStepNext}
          isDisable={!!!videoURL || isLoadingUploadVideo}
          isLoading={isLoadingUploadCard}
        ></StepsProgress>
      ) : null}
    </div>
  );
  // <div>
  //   {progressUploadCard >= 1 && progressUploadCard < 100 ? (
  //     <Spinner></Spinner>
  //   ) : (
  //     <Fragment>
  //       <h1>Video added</h1>
  //       <QRCode
  //         logoImage="/logo-letter.png"
  //         logoWidth={40}
  //         logoHeight={40}
  //         logoOpacity={1}
  //         fgColor="#353535"
  //         qrStyle="squares"
  //         value={`https://mmry-fe.vercel.app/card/${cardId}`}
  //       />
  //       <Button
  //         variant="solid"
  //         className=" mt-3 w-[170px]"
  //         onClick={() => window.location.reload()}
  //       >
  //         Open video
  //       </Button>
  //     </Fragment>
  //   )}
  // </div>
};
