'use client';

import React from 'react';
import Link, { default as NextLink } from 'next/link';

import { useState } from 'react';
import Image from 'next/image';
import { Logo } from 'src/icons/header/logo.icon';

interface Link {
  title: string;
  href: string;
}
const navigationLinks: Link[] = [
  {
    title: 'Over ons',
    href: 'https://www.mmry.nl/',
  },
  {
    title: 'Contact',
    href: 'https://www.mmry.nl/contact/',
  },
];

type Props = {};

const renderLinks = (links: Link[]) => {
  return links.map((link) => (
    <li key={link.href}>
      <Link className="text-2xl md:text-base text-white" href={`${link.href}`}>
        {link.title}
      </Link>
    </li>
  ));
};
function Header({}: Props) {
  const [isOpen, setIsOpen] = useState(true);
  return (
    <header className="fixed whitespace-nowrap top-0 left-0 bg-primary-500 backdrop-blur-xl w-full h-[60px] items-center flex z-20">
      <div className="container grid grid-cols-2 lg:grid-cols-3 items-center  justify-between">
        {/* <NextLink href="/"> */}
          <span className="flex items-center text-2xl font-medium h-[40px]">
            <Image
            alt={"MMRY's logo"}
              src="/white-logo.png"
              objectFit="contain"
              width={90}
              height={40}
            ></Image>
          </span>
        {/* </NextLink> */}
        <nav
          className={`md:justify-self-end lg:justify-self-center top-[60px] flex justify-center md:flex-row  duration-500 md:static absolute   bg-white/80 md:bg-transparent md:min-h-fit h-[100vh] md:h-auto left-0  md:w-auto  w-full flex- items-center px-5 ${
            isOpen ? 'left-[-100%]' : 'left-[0px]'
          }`}
        >
          <ul className=" bg-white md:bg-transparent w-fit shadow-xl md:shadow-none md:h-auto flex flex-col p-20 md:p-0 items-center justify-center md:flex-row gap-10 md:gap-6 mb-24 md:mb-0 rounded-xl">
            {/*<li>*/}
            {/*  <Link variant="secondary" href="/blog">*/}
            {/*    Blog*/}
            {/*  </Link>*/}
            {/*</li>*/}

            {renderLinks(navigationLinks)}
          </ul>
        </nav>
        {/* Hamburger Icon */}
        <div
          onClick={() => setIsOpen((isOpen) => !isOpen)}
          className="justify-self-end block md:hidden relative  cursor-pointer lg:hidden"
        >
          <div
            aria-hidden="true"
            className={`${
              !isOpen && 'rotate-45 translate-y-2'
            } m-auto h-1 w-7 bg-white rounded bg-accent-foreground transition duration-300`}
          ></div>
          <div
            aria-hidden="true"
            className={`${
              !isOpen && '-rotate-45 -translate-y-1'
            } m-auto mt-2 h-1 w-7 bg-white rounded bg-accent-foreground transition duration-300`}
          ></div>
        </div>
      </div>
    </header>
  );
}

export default Header;
