import Image from 'next/image';
import React from 'react';
interface ErrorBoundaryProps {
  title: string;
  description: string;
  image?: string;
}
export const ErrorBoundary = ({
  title,
  description,
  image = '/error/error-not-found.png',
}: ErrorBoundaryProps) => {
  return (
    <div className="flex flex-col justify-center container text-center py-16 items-center">
      <h1 className="text-3xl font-bold text-shadow-800 mb-3">{title}</h1>
      <p className="text-md text-shadow-500">{description}</p>
      <div>
        <Image
          src={image}
          alt={'error'}
          width={360}
          height={200}
        ></Image>
      </div>
    </div>
  );
};
