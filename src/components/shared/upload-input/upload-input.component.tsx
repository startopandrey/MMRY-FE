import { useUploadFileToCDY } from '@api/api-client';
import FileButton, {
  FileDataButton,
} from '@components/UI/file-button.component';
import ReactPlayer from 'react-player';
import { Card, CardHeader } from '@nextui-org/react';
import Image from 'next/image';

export default function UploadInput({ presetVarants, files, onChange }: any) {
  const { mutate: mutateUploadFile } = useUploadFileToCDY();

  const onChangeFiles = (file: FileDataButton) => {
    console.log(file.type);
    if (!file.id) return;
    const currentPreset = presetVarants[file?.type];
    if (!currentPreset) return;
    mutateUploadFile(
      {
        file: file.data,
        preset: currentPreset,
        resourceType: file.type,
      },
      {
        onSuccess(data: any, variables, context) {
          if (data?.url) {
            onChange({ ...file, uri: data?.url });
          }
        },
      }
    );
  };
  const fileToSrc = (file: File): any => {
    if (file) {
      return URL.createObjectURL(file);
    }
  };
  console.log({ files }, 23);

  return (
    <div className="flex flex-col items-center justify-center  text-center dark:bg-gray-800">
      <div className="w-full mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-lg dark:border-gray-700">
        <div className="text-center">
          <FileButton accept="image/*" onChange={onChangeFiles}></FileButton>

          <p className="text-xs text-gray-500 dark:text-gray-400">
            PNG, JPG, GIF up to 10MB
          </p>
        </div>
      </div>

      <div className="mt-5 w-full">
        <Card className="">
          <CardHeader>
            <h1>File Preview</h1>
          </CardHeader>
          <div className="p-5 max-h-[300px] overflow-scroll">
            {files.length ? (
              files?.map((file: any) => {
                // const uri = fileToSrc(data);
                console.log({ file });
                if (file.uri && file.type == 'image') {
                  return (
                    <Image
                      key={file.uri}
                      src={file.uri}
                      alt="File Preview"
                      width={400}
                      height={400}
                      className="w-full object-cover rounded-lg"
                    />
                  );
                }
                if (file.uri && file.type == 'video') {
                  return (
                    <div className="w-[100%] max-h-[50%]  h-[150px] rounded-lg  relative mb-2">
                      <ReactPlayer
                        key={file.uri}
                        style={{
                          display: 'flex',
                          justifyContent: 'center',
                        }}
                        url={file.uri}
                        controls={true}
                        light={false}
                        pip={true}
                      />
                    </div>
                  );
                }
              })
            ) : (
              <h5 className="mb-4">No images were selected</h5>
            )}
          </div>
        </Card>
      </div>
    </div>
  );
}
