import { CldVideoPlayer } from 'next-cloudinary';

export const VideoPlayer = ({ url }: { url: string }) => {

    if (!url) {
      return null;
    }
    return (
      <CldVideoPlayer
        logo={false}
        width={'100%'}
        height={'50%'}
        src={url}
        colors={{
          base: '#3ba5de',
          text: '#fff',
          accent: '#fff',
        }}
      />
    );
  };
 