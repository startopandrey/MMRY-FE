import React, { Fragment } from 'react';
import { IconButton } from 'src/components/UI/icon-button.component';
import { ArrowIcon } from 'src/icons/arrow.icon';
import { Button } from 'src/components/UI/button.component';
interface StepsProgressProps {
  isFinished: boolean;
  progress: number;
  onClickBack: () => void;
  onClickNext: () => void;
  isDisable?: boolean;
  isLoading?: boolean;
  selectedIndex?: number;
}
export const StepsProgress = ({
  isFinished,
  progress,
  onClickBack,
  onClickNext,
  isDisable,
  isLoading,
  selectedIndex,
}: StepsProgressProps) => {
  return (
    <div className=" pb-12">
      <div className="my-4">
        <div className="w-full flex justify-center">
          <div className="relative">
            <span
              style={{ width: `${progress}px` }}
              className={
                'top-[-5px] left-[-75px] absolute h-[6px] rounded-lg z-10 bg-primary-500 transition-all duration-200'
              }
            ></span>

            <span className="top-[-5px] left-[-75px] absolute w-[160px] h-[6px] rounded-lg  bg-primary-100"></span>
          </div>
        </div>{' '}
      </div>
      <div className="my-4 flex items-center gap-4">
        {isFinished ? (
          <>
            {selectedIndex != 0 ? (
              <IconButton
                icon={<ArrowIcon></ArrowIcon>}
                size={'lg'}
                onClick={onClickBack}
              ></IconButton>
            ) : null}

            <Button
              className="w-full text-white"
              variant="solid"
              isDisabled={isDisable}
              isLoading={isLoading}
              onClick={onClickNext}
            >
              {selectedIndex === 1 ? 'Afronden' : 'Volgende'}
            </Button>
          </>
        ) : (
          <Button
            className="w-full text-white"
            variant="solid"
            isDisabled={isDisable}
            isLoading={isLoading}
            onClick={onClickNext}
          >
            Afronden
          </Button>
        )}
      </div>
    </div>
  );
};
