'use client';

import { splitAtIndex } from '@helpers/pdf.helper';
import React, { useEffect, useRef } from 'react';
import { QR } from 'react-qr-rounded';
import { useRefToImage } from 'src/hooks/useRefToImage.hook';

export const PDFQrcode = ({
  addQrcodeToImages,
  qrcodeId,
  password,
}: {
  addQrcodeToImages: (imageURL: string) => void;
  qrcodeId: string;
  password: number;
}) => {
  const div = useRef(null);
  const imageURL = useRefToImage(div);
  const transformedPassword = splitAtIndex(password, 3)
  useEffect(() => {
    console.log(imageURL);
    if (imageURL) {
      addQrcodeToImages(imageURL);
    }
  }, [imageURL, addQrcodeToImages]);

  return (
    <div ref={div} className=" bg-transparent w-[200px] h-fit text-center">
      <div className=''>
        <QR
          color="#454545"
          backgroundColor="transparent"
          rounding={80}
          cutout
          cutoutElement={
            // eslint-disable-next-line @next/next/no-img-element
            <img
              src="/logo-letter.png"
              alt={'QR code'}
              style={{
                objectFit: 'contain',
                width: '100%',
                height: '100%',
              }}
            />
          }
          errorCorrectionLevel="H"
        >
          {`https://mmry-fe.vercel.app/card/${qrcodeId}`}
        </QR>
      </div>
      {transformedPassword ? <h2 className="!leading-[80%] !text-nowrap mt-3 text-[2.8rem] text-shadow-800">{`${transformedPassword}`}</h2> : null}
    </div>
  );
};

export default PDFQrcode;
