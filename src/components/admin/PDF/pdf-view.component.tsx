'use client';

import React, { useCallback, useState } from 'react';
import {
  Document,
  Page,
  Text,
  View,
  StyleSheet,
  PDFViewer,
  PDFDownloadLink,
  Image,
} from '@react-pdf/renderer';

import { Button } from 'src/components/UI/button.component';
import PDFQrcode from './pdf-qrcode.component';
import { QRcode } from './pdf-container.component';
// Define styles
const styles = StyleSheet.create({
  page: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 400,
    flexDirection: 'row',
    backgroundColor: '#ffffff',
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
  },
  heading: {
    fontSize: 24,
    marginBottom: 10,
  },
  paragraph: {
    fontSize: 12,
    marginBottom: 10,
  },
  qrCodeContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  qrCode: {
    // marginLeft: 'auto',
    // marginRight: 'auto',
    width: '100%',
    height: '100%',
    objectFit: 'contain',
  },
});

// Create PDF Component
const PDFComponent = ({ images }: { images: string[] }) => {
  // Convert CustomQRCode component to string
  return (
    <Document>
      {images.map((el) => (
        <Page wrap={false} key={el} style={styles.page}>
          <View style={styles.qrCodeContainer}>
            {el ? <Image style={styles.qrCode} src={el}></Image> : null}
          </View>
        </Page>
      ))}
    </Document>
  );
};

export const PDFView = ({ qrcodes }: { qrcodes: QRcode[] }) => {
  const [qrcodeImages, setQrcodeImages] = useState<Array<string> | null>(null);

  const addQrcodeToImages = useCallback(
    (image: any) => {
      console.log(image);
      setQrcodeImages((state) => (state ? [...state, image] : [image]));
    },
    [setQrcodeImages]
  );
  console.log(qrcodeImages);
  return (
    <div className="w-full flex flex-col justify-center items-center">
      {qrcodeImages?.length == qrcodes.length ? (
        <div className="w-full flex flex-col gap-6 items-center mb-8">
          <PDFViewer style={{ width: '100%', height: '90vh' }}>
            <PDFComponent images={qrcodeImages} />
          </PDFViewer>
          <Button className=" w-44">
            {' '}
            <PDFDownloadLink
              document={<PDFComponent images={qrcodeImages} />}
              fileName="mmry-qrcodes.pdf"
            >
              {({ blob, url, loading, error }) =>
                loading ? 'Loading document...' : 'Download PDF'
              }
            </PDFDownloadLink>
          </Button>
        </div>
      ) : null}
      <div className="w-full h-[300px]  overflow-x-scroll">
        <div className="flex w-fit gap-6">
          {qrcodes
            ? qrcodes.map((qrcode) => (
                <PDFQrcode
                  key={qrcode.id}
                  qrcodeId={qrcode.id}
                  addQrcodeToImages={addQrcodeToImages}
                  password={qrcode.password}
                ></PDFQrcode>
              ))
            : null}
        </div>
      </div>
    </div>
  );
};
