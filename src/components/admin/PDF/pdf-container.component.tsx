'use client';

import React, { useCallback, useState } from 'react';
import { Input, Select, SelectItem } from '@nextui-org/react';
import { Button } from '../../UI/button.component';
import { useCreateCard, useCreateCards } from '@api/api-client';
import { PDFView } from './pdf-view.component';
import { generateSixDigitNumber } from '../../../helpers/pdf.helper';

export interface QRcode {
  id: string;
  password: number;
}
interface QuantityVariant {
  quantity: number;
}
const quantityVariants: QuantityVariant[] = [
  { quantity: 1 },
  { quantity: 2 },
  { quantity: 5 },
  { quantity: 10 },
  { quantity: 20 },
  { quantity: 50 },
];
export const PDFContainer = () => {
  //state
  const [qrcodes, setQrcodes] = useState<QRcode[] | null>(null);
  const [selectedQuantity, setSelectedQuantity] = useState('');
  const [newQRcodeId, setNewQRcodeId] = useState<{
    id: string;
    password: string;
  } | null>(null);
  console.log(qrcodes);
  // hooks
  // const {
  //   mutate: mutateCard,
  //   isSuccess,
  //   isLoading: isLoadingCard,
  //   error,
  // } = useCreateCard();
  const {
    mutate: mutateCreateCards,
    isSuccess,
    isLoading,
    error,
  } = useCreateCards();
  // memo
  const createCards = (quantity: number) =>
    mutateCreateCards(quantity, {
      onSuccess(data) {
        console.log({ data });
        const transformedCards = data.map(
          (card: { _id: string; password: number }) => ({
            id: card._id,
            password: card.password,
          })
        );
        setQrcodes(transformedCards);
      },
    });

  // handles
  const handleCreateCards = () => {
    if (selectedQuantity) {
      createCards(Number(selectedQuantity));
      setSelectedQuantity('');
    }
  };
  const onChangeQuantity = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedQuantity(e.target.value);
  };
  return (
    <div className="flex flex-col justify-center items-center  container my-20">
      {qrcodes ? <PDFView qrcodes={qrcodes}></PDFView> : null}
      <div className="w-64 my-12">
        {/* <Input
          className="my-4"
          type="text"
          label="Card ID"
          value={newQRcodeId?.id ?? ''}
          onValueChange={handleChangeID}
        /> */}
        {error?.message && (
          <h3 className="text-center text-base text-danger-500">
            {error?.message}
          </h3>
        )}
        {isSuccess && (
          <h3 className="text-center text-base text-green-500">
            {'Success, card created!'}
          </h3>
        )}
        <Select
          variant="flat"
          size="lg"
          labelPlacement={'outside'}
          value={selectedQuantity}
          onChange={onChangeQuantity}
          label="Select quantity"
          className="max-w-xs mb-4"
        >
          {quantityVariants.map((animal) => (
            <SelectItem key={animal.quantity} value={animal.quantity}>
              {`${animal.quantity}`}
            </SelectItem>
          ))}
        </Select>
        <Button
          isLoading={isLoading}
          isDisabled={!selectedQuantity}
          className="w-full mt-1"
          variant="solid"
          onClick={handleCreateCards}
        >
          Create Cards
        </Button>
      </div>
    </div>
  );
};
