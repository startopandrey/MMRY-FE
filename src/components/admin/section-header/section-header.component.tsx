import { ReactNode } from 'react';

export default function SectionHeader({
  title,
  endContent,
}: {
  title: string;
  endContent?: ReactNode;
}) {
  return (
    <div className="mb-10 flex justify-between">
      <h4 className="h4 font-bold">{title}</h4>
      {endContent && endContent}
    </div>
  );
}
