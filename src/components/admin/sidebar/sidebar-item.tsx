import NextLink from 'next/link';
import React from 'react';
import clsx from 'clsx';

interface Props {
  title: string;
  icon: React.ReactNode;
  isActive?: boolean;
  href?: string;
}

export const SidebarItem = ({ icon, title, isActive, href = '' }: Props) => {
  console.log(isActive);
  return (
    <NextLink
      href={`/admin/${href}`}
      className="text-default-900 active:bg-none w-[200px]"
    >
      <div
        className={clsx(
          'flex gap-2 w-full min-h-[44px] h-full items-center px-6 rounded-xl cursor-pointer transition-all duration-150 active:scale-[0.98] ',
          isActive
            ? 'bg-primary-100 [&_svg_path]:fill-primary-500'
            : 'hover:bg-default-100 [&_svg_path]:fill-shadow-500'
        )}
      >
        {icon}
        <span className={isActive ? 'text-primary-500 font-semibold' : 'text-default-900'}>
          {title}
        </span>
      </div>
    </NextLink>
  );
};
