import { Logo } from '@icons/header/logo.icon';
import React from 'react';

function CompanyHolder() {
  return (
    <div className="flex flex-col items-center gap-2 mt-4">
      <div className="flex flex-col gap-3">
        <Logo />
        <span className="text-xs font-medium text-shadow-500">
          CEO | Bart van Dam
        </span>
      </div>
    </div>
  );
}

export default CompanyHolder;
