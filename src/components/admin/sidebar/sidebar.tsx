'use client';

import React from 'react';
import { usePathname } from 'next/navigation';
import {
  GraphIcon,
  MegaphoneIcon,
  PeopleIcon,
  ReportIcon,
  VersionsIcon,
} from '@primer/octicons-react';
import { SidebarItem } from './sidebar-item';
import { SidebarMenu } from './sidebar-menu';
import { SettingsIcon } from '@icons/sidebar/settings-icon';
import { ChangeLogIcon } from '@icons/sidebar/changelog-icon';

import { Sidebar } from './sidebar.styles';
import CompanyHolder from './company-holder';

export const SidebarWrapper = () => {
  const pathname = usePathname();

  return (
    <aside className="h-screen z-[10] w-[230px] sticky top-0 border-r-1 border-shadow-50">
      <div>
        <div className={Sidebar.Header()}>
          {' '}
          <CompanyHolder />{' '}
        </div>
        <div className="flex flex-col justify-between h-full">
          <div className={Sidebar.Body()}>
            <SidebarItem
              title="Dashboard"
              icon={<GraphIcon size={22} />}
              isActive={pathname.includes('/admin/dashboard')}
              href="/dashboard"
            />
            <SidebarMenu title="Main Menu">
              <SidebarItem
                isActive={ pathname.includes('/admin/memory-cards')}
                title="Memory Cards"
                icon={<VersionsIcon size={22} />}
                href="/memory-cards"
              />
              <SidebarItem
                isActive={ pathname.includes('/admin/members')}
                title="Users"
                icon={<PeopleIcon size={22} />}
                href="/users"
              />
              <SidebarItem
                isActive={pathname.includes('/admin/marketing')}
                title="Marketing"
                icon={<MegaphoneIcon size={22} />}
                href="/marketing"
              />
              <SidebarItem
                isActive={pathname.includes('/admin/reports')}
                title="Reports"
                icon={<ReportIcon size={22} />}
              />
            </SidebarMenu>

            <SidebarMenu title="General">
              <SidebarItem
                isActive={pathname === '/settings'}
                title="Settings"
                icon={<SettingsIcon />}
              />
            </SidebarMenu>

            <SidebarMenu title="Updates">
              <SidebarItem
                isActive={pathname === '/logs'}
                title="Server Logs"
                icon={<ChangeLogIcon />}
              />
            </SidebarMenu>
          </div>
        </div>
      </div>
    </aside>
  );
};
