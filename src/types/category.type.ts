import { MediaAsset } from './memories.type';

interface IWidget {
  primary: string;
}

export interface ICategoryDB {
  _id: string;
  id: string;
  title: string;
  widget: IWidget;
  assets: MediaAsset[];
  itemCount: number;
  isPopularCategory: boolean;
}
