export type CDYPresets =
  | 'category_images'
  | 'profile_avatars'
  | 'memories_images'
  | 'memories_videos';

export type MediaType = 'video' | 'image';

export type PresetVariants =
  | {
      name: 'category_images';
      type: 'image';
    }
  | {
      name: 'profile_avatars';
      type: 'image';
    }
  | {
      name: 'memories_images';
      type: 'image';
    }
  | {
      name: 'memories_videos';
      type: 'video';
    };
