
type AssetType = 'video' | 'image';

export interface MediaAsset {
  _id: string;
  id?: string;
  type: AssetType;
  fileName: string;
  localUri?: string;
  size?: number | string;
  uri: string;
  remoteUri?: string;
}

