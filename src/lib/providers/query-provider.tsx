'use client';

import { ReactNode } from 'react';
import { QueryClientProvider } from 'react-query';
import queryClient from '@api/query-client';
interface QueryProviderProps {
  children: ReactNode;
}
export const QueryProvider = ({ children }: QueryProviderProps) => {
  return (
    <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
  );
};
