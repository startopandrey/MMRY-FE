import { ReactNode } from 'react';

import { ClerkProvider, auth } from '@clerk/nextjs';
import { SyncActiveOrganization } from '@components/sync-active-organization';
import { QueryProvider } from './query-provider';

interface GlobalProvidersProps {
  children: ReactNode;
}
export const GlobalProviders = ({ children }: GlobalProvidersProps) => {
  const { sessionClaims } = auth();
  console.log({ sessionClaims });
  return (
    <ClerkProvider>
      <SyncActiveOrganization membership={sessionClaims?.membership} />
      <QueryProvider>
        {children}
        </QueryProvider>
    </ClerkProvider>
  );
};
