import React from 'react';

export const ArrowIcon = () => {
  return (
    <svg
      width="17"
      height="17"
      viewBox="0 0 17 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M4.0777 7.2047H16.98V9.3247H4.0777L9.76354 15.0105L8.2647 16.5094L0.0200195 8.2647L8.2647 0.0200195L9.76354 1.51886L4.0777 7.2047Z"
        fill="#353535"
      />
    </svg>
  );
};
