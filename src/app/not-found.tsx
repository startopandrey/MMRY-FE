export default function NotFound() {
  return (
    <div className="flex flex-col justify-center items-center mt-12">
      <h1 className="text-3xl font-semibold">Page not found</h1>
      <p>Could not found requested resource</p>
    </div>
  );
}
