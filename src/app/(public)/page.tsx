import { Button } from '@nextui-org/button';
import { Metadata } from 'next';
import React from 'react';

export const metadata: Metadata = {
  title: '',
};

export default function Web() {

  return (
    <>
      <section className="bg-white dark:bg-gray-900">
        <h1>MMRY</h1>
      </section>
    </>
  );
}
