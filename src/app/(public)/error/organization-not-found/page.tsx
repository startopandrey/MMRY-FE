
import { auth } from '@clerk/nextjs';

const OrganizationNotFound = () => {
  const { userId } = auth();
  return (
    <div>
      <h1>
        {`No selected organization, ask admin to add user with ID: ${userId} in organization list`}
      </h1>
    </div>
  );
};
export default OrganizationNotFound;
