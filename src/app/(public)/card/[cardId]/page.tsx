'use client';

import { Spinner } from '@nextui-org/react';
import React from 'react';


import { useGetCardType } from '@api/api-client';
import { useRouter } from 'next/navigation';
import { CardUpload } from '@components/card/card-upload/card-upload.component';
import { CardPlay } from '@components/card/card-play/card-play.components';
import { ErrorBoundary } from '@components/error/error-boundary.component';

interface CardProps {
  params: { cardId: string };
}

export default function CardPage({ params: { cardId } }: CardProps) {
  const router = useRouter();
  const {
    data: cardTypeData,
    isLoading,
    error: errorCardType,
    isError,
  } = useGetCardType(cardId);

  if (isError) {
    return (
      <ErrorBoundary
        title={'Oeps..!'}
        description={'Scan de code opnieuw. Of de code is ongeldig'}
      ></ErrorBoundary>
    );
  }
  if (isLoading) {
    return (
      <div className="flex justify-center items-center w-full mt-56">
        <Spinner></Spinner>
      </div>
    );
  }
  return (
    <>
      <section className="flex relative h-[calc(100vh-60px)] overflow-y-scroll  big-blue-radial-gradient-background ">
        {cardTypeData?.type == 'PLAY' ? (
          <CardPlay cardId={cardId}></CardPlay>
        ) : (
          <CardUpload cardId={cardId}></CardUpload>
        )}
      </section>
    </>
  );
}
