'use client';

import { Spinner } from '@nextui-org/react';
import React from 'react';

import { useGetCardType } from '@api/api-client';
import { useRouter } from 'next/navigation';
import { ErrorBoundary } from '@components/error/error-boundary.component';
import { useContest } from 'src/features/Contests/services/index.service';
import Image from 'next/image';
import { Button } from '@components/UI/button.component';

export default function ContestPage({ params: { id }}: any) {
  const router = useRouter();
  const {
    data: contest,
    isLoading,
    error,
    isError,
  } = useContest(id?.toString());

  if (isError) {
    return (
      <ErrorBoundary
        title={'Oeps..!'}
        description={'Scan de code opnieuw. Of de code is ongeldig'}
      ></ErrorBoundary>
    );
  }
  if (isLoading) {
    return (
      <div className="flex justify-center items-center w-full mt-56">
        <Spinner></Spinner>
      </div>
    );
  }

  const navigateToApp = () =>
    (window.location.href = `mmry://(auth)/(modals)/contests/${id}`);

  return (
    <>
      <section className="flex relative h-[calc(100vh-60px)] overflow-y-scroll  big-blue-radial-gradient-background justify-center items-center">
        <div className=" bg-white w-fit h-fit p-4 rounded-3xl flex flex-col items-center mb-10">
          <Image
            width={500}
            height={500}
            src={contest.assets[0].uri}
            className="w-[300px] h-[300px] object-cover rounded-2xl mb-3"
            alt="contest"
          ></Image>
          <h3 className="h5 font-bold mb-3">{contest.title}</h3>
          <Button color="primary" variant="primary" onPress={navigateToApp}>
            Join the contest
          </Button>
        </div>
      </section>
    </>
  );
}
