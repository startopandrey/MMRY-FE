import Header from '@components/header/header.component';
import { ReactNode } from 'react';

interface PublicLayout {
    children: ReactNode
}
const PublicLayout = ({ children }: PublicLayout) => {
  return (
    <>
      <Header></Header>
      <main className="mt-[60px]  font-semibold text-shadow-900">
        {children}
      </main>
    </>
  );
};
export default PublicLayout