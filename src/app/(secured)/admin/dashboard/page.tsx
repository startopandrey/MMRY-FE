export default function Dashboard() {
  return (
    <div className="flex justify-center items-center flex-1">
      <h1 className="text-xl font-semibold text-center">Dashboard</h1>
    </div>
  );
}
