
import CreateCategoryModal from 'src/features/Categories/components/create-category-modal.component';
import CreateContestModal from 'src/features/Contests/components/create-contest-modal.component';
import SectionHeader from '@components/admin/section-header/section-header.component';
import { Button } from '@components/UI/button.component';
import Image from 'next/image';
import React from 'react';
import CategoriesCardRow from 'src/features/Categories/components/categories-card-row.component';
import Categories from 'src/features/Categories/components/categories.component';
import Contests from 'src/features/Contests/components/contests.component';

export default function Marketing() {
  return (
    <div className="container mt-8">
      <div className='mb-24'>
        <SectionHeader
          title={'Discovery categories'}
          endContent={<CreateCategoryModal></CreateCategoryModal>}
        ></SectionHeader>
        <div className="mt-5">
          <h1 className="text-xl font-semibold mb-4">
            Categories:
          </h1>
        </div>
       <Categories />
      </div>
      <div>
        <SectionHeader
          title={"Contest's:"}
          endContent={<CreateContestModal></CreateContestModal>}
        ></SectionHeader>
        <div className="mt-5">
          <h1 className="text-xl font-semibold mb-4">
            Current Contest&apos;s:
          </h1>
          <Contests></Contests>

        </div>
      </div>
    </div>
  );
}
