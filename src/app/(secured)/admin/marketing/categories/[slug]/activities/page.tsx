'use client';

import Activities from 'src/features/Activities/components/activities.component';
import CreateActivityModal from 'src/features/Activities/components/create-activity-modal.component';
import SectionHeader from '@components/admin/section-header/section-header.component';
import React from 'react';
import { useCategory } from 'src/features/Categories/services/index.service';

interface IndexProps {
  params: { slug: string };
}
const Index = ({ params: { slug } }: IndexProps) => {
  const { data: category } = useCategory(slug);
  
  console.log(category)
  return (
    <div className="container mt-8">
      <div className="mb-24">
        <SectionHeader
          title={category?.title ?? ''}
          endContent={<CreateActivityModal></CreateActivityModal>}
        ></SectionHeader>
        <div className="mt-5">
          <h1 className="text-xl font-semibold mb-4">Activities:</h1>
          <Activities data={category?.activities}></Activities>
        </div>
      </div>
    </div>
  );
};

export default Index;
