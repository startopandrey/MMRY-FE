import { PDFContainer } from '@components/admin/PDF/pdf-container.component';

export default function MemoryCards() {
  return (
    <div className="flex justify-center items-center flex-1">
      <PDFContainer></PDFContainer>
    </div>
  );
}
