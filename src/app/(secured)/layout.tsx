import { SidebarWrapper } from '@components/admin/sidebar/sidebar';
import { ReactNode } from 'react';

const AdminLayout = ({ children }: { children: ReactNode }) => {
  return (
    <section className="flex">
                <SidebarWrapper />
      {children}
    </section>
  );
};
export default AdminLayout;
