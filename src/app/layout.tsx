// 'use client';

import 'src/styles/tailwind.css';

import { Nunito } from 'next/font/google';

import Script from 'next/script';
import { GlobalProviders } from 'src/lib/providers/global-providers';

const nunito = Nunito({ subsets: ['latin'], display: 'swap' });

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <GlobalProviders>
      <html lang="en">
        <head>
          <meta charSet="utf-8" />

          <link rel="icon" href="/logo-letter.png" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta name="theme-color" content="#000000" />
          <title>MMRY</title>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta name="apple-itunes-app" content="app-id=6502915900" />
          <meta name="description" content="MMRY" />
          <Script
            src="https://widget.cloudinary.com/v2.0/global/all.js"
            type="text/javascript"
          ></Script>
        </head>

        <body className={nunito.className}>{children} </body>
      </html>
    </GlobalProviders>
  );
}
