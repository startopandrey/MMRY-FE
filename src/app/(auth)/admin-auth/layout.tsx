import { SidebarWrapper } from '@components/admin/sidebar/sidebar';
import { ReactNode } from 'react';
interface AdminLayout {
  children: ReactNode
}
const AdminLayout = ({ children }: AdminLayout) => {
  return (
    <section className="">
      {children}
    </section>
  );
};
export default AdminLayout;
