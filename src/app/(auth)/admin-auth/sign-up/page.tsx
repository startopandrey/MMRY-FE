'use client';

import { SignUp } from '@clerk/nextjs';
import React, {  } from 'react';


const SignUpPage = () => {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-gray-50">
      {/* <div className="w-full max-w-md p-6 bg-white rounded-2xl shadow-md">
        <h2 className="text-3xl font-semibold text-center text-gray-800 mb-4">
          Register
        </h2>
        <Spacer y={2} />
        <Input
          startContent={<User className="w-5 h-5" />}
          placeholder="Username"
          onValueChange={setUsername}
          type="text"
          className="mb-4"
        />
        <Input
          startContent={<Lock className="w-5 h-5" />}
          placeholder="Password"
          onValueChange={setPassword}
          type="password"
          className="mb-4"
        />
        <Button
          variant="solid"
          color="primary"
          size="lg"
          fullWidth
          isLoading={isLoadingRegister}
          onClick={handleSubmitRegister}
          className="my-3"
        >
          Submit
        </Button>
        <Spacer y={1} />
        <h3 className="text-center">
          {' '}
          Have an account?{' '}
          <Link className={'text-primary-500'} href="/admin/login">
            Login
          </Link>
        </h3>
      </div> */}
       <SignUp />
    </div>
  );
};

export default SignUpPage;
