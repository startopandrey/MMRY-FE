export const CDY_PRESETS_VARIANTS = {
  category: {
    image: 'category_images',
  },
  profile: {
    image: 'profile_avatars',
  },
  memory: {
    image: 'memories_images',
    video: 'memories_videos',
  },
  contest: {
    image: 'contest_images',
    video: 'contest_videos',
  },
  activity: {
    image: 'activity_images',
    video: 'activity_videos',
  },
};
