import { api } from '@api/api-client';
import axios, { AxiosResponse } from 'axios';
import { useRouter } from 'next/navigation';

import { useMutation, useQuery } from 'react-query';
import { useEffect, useState } from 'react';

export const useAuth = () => {
  const router = useRouter();
  const [isAuth, setIsAuth] = useState(false);
  useEffect(() => {
    const token = localStorage.getItem('token');

    if (!token) {
      setIsAuth(false);
      router.push('/admin/login');
    }
    const config = {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    };
    api
      .get('/user/verifyToken', config)
      .then(() => {
        // Token is valid
        console.log('valid');
        setIsAuth(true);
      })
      .catch((err) => {
        // Token is invalid or expired
        router.push('/admin/login');
      });
  }, []);

  return { isAuth }; // This hook doesn't render any UI
};
