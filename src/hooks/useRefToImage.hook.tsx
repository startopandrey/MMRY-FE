import { toPng } from 'html-to-image';
import React, { useEffect, useState } from 'react';

export const useRefToImage = ( ref: any ): string | null => {
  const [image, setImage] = useState<string | null>(null);
  useEffect(() => {
    const convertQRCodeToPNG = async () => {
      try {
        if (ref.current) {
          // console.log(qrCodeRef.current.canvas.current)
          const dataUrl = await toPng(ref.current);
          setImage(dataUrl);
        }
      } catch (error) {
        console.error('Error converting QR code to PNG:', error);
      }
    };

    convertQRCodeToPNG();
  }, []); // Only run once on component mount

  if (!image) {
    return null;
  }
  return image;
};
