export interface TextWithLink {
  text: string;
  href?: string;
}

export type Section = {
  sectionId: string;
  title: string;
  content?: string;
  list?: Array<string>;
  listWithLinks?: Array<TextWithLink[]>;
};

interface CardPlayAdvertise {
  sections: Array<Section>;
}

export const cardPlayAdvertise: CardPlayAdvertise = {
  sections: [
    {
      sectionId: 'advertise',
      title: 'advertise',
      listWithLinks: [
        [
          { text: 'Deze video is ' },
          { text: '3 maanden' },
          {
            text: 'beschikbaar via je QR-code.',
          },
        ],
        [
          {
            text: 'Wil je deze MMRY langer bewaren? Klik op onderstaande knop.',
          },
        ],
        [
          {
            text: 'Binnenkort verschijnt onze App maar je kunt je nu alvast aanmelden en dan zorgen wij ervoor dat dit filmpje in je account komt te staan.',
          },
        ],
        [
          { text: 'Ook krijg je ' },
          { text: '(als vroege vogel)', href: 'www.mmry.nl/vroege-vogels' },
          { text: ' een coupon om GRATIS een ' },
          { text: 'MMRY kaartje ', href: 'https://www.mmry.nl/' },
          {
            text: ' te versturen.',
          },
        ],
        [{ text: 'Download gratis onze MMRY app.' }],
      ],
    },
  ],
};
