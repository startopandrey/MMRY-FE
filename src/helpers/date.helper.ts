import { ZonedDateTime } from '@internationalized/date';

export const transformDateToISO = (date: ZonedDateTime) => {
  const zonedDateTime = new ZonedDateTime(
    date.year,
    date.month,
    date.day,
    'Europe/Amsterdam',
    0,
    date.hour,
    date.minute
  )
  console.log(zonedDateTime.toString());
  return zonedDateTime.toAbsoluteString();
};
