export const generateSixDigitNumber = () => {
  const min = 100000; // Smallest 6-digit number (100000)
  const max = 999999; // Largest 6-digit number (999999)
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const splitAtIndex = (value: string | number, index: number) => {
  const stringValue = value.toString()
  return stringValue.substring(0, index) + ' ' + stringValue.substring(index);
};
