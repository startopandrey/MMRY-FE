export const transformMedia = (media: any)=> {
    const transformedMedia = {...media, fileName: media.name ?? ''}
    delete media.data
  return transformedMedia
}