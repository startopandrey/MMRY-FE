import { Link } from '@nextui-org/react';
import React from 'react';

const ContestCardRow = ({ title, id }: any) => {
  return (
    <div className="flex flex-col">
      <Link
        isBlock
        showAnchorIcon
        isDisabled={true}
        color="primary"
        className="w-fit"
      >
        {title}
      </Link>
    </div>
    // <div className=" bg-neutral-100 w-fit p-4 rounded-3xl">
    //   <Image
    //     width={500}
    //     height={500}
    //     src={'/consert.jpg'}
    //     className="w-[300px] h-[300px] object-cover rounded-2xl mb-3"
    //     alt="contest"
    //   ></Image>
    //   <h3 className="h5 font-bold">Contest</h3>
    //   <p className="mb-3 text-neutral-500">5 days left</p>
    //   <Button variant="danger">Delete</Button>
    // </div>
  );
};
export default ContestCardRow;
