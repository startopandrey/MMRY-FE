'use client'

import React from 'react';
import ContestCardRow from './contest-card-row.component';
import { useContests } from '../services/index.service';

const Contests = () => {
  const { data: contests } = useContests();
  return (
    <div>
      {contests?.map((activity: any) => (
        <ContestCardRow
          key={activity.id}
          id={activity.id}
          title={`${activity.title}-${activity.id}`}
        ></ContestCardRow>
      ))}
    </div>
  );
};

export default Contests;
