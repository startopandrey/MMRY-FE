'use client';

import { Button } from '@components/UI/button.component';
import UploadInput from '@components/shared/upload-input/upload-input.component';
import {
  getLocalTimeZone,
  now,
  parseDate,
  parseZonedDateTime,
  ZonedDateTime,
} from '@internationalized/date';

import {
  Checkbox,
  DatePicker,
  DateRangePicker,
  DateValue,
  Input,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  RangeValue,
  Select,
  SelectItem,
  Textarea,
  useDisclosure,
} from '@nextui-org/react';
import { useRouter } from 'next/navigation';
import React, { useCallback, useState } from 'react';
import { useQueryClient } from 'react-query';
import { useCategories } from 'src/features/Categories/services/index.service';
import { transformMedia } from 'src/features/helper/index.helper';
import { ICategoryDB } from 'src/types/category.type';
import { CDY_PRESETS_VARIANTS } from 'src/utils/constants.utils';
import { useCreateContest } from '../services/index.service';
import { transformDateToISO } from '@helpers/date.helper';

const CreateContestModal = () => {
  const router = useRouter();
  const queryClient = useQueryClient();
  const {
    mutate: mutateCreateContest,
    isLoading,
    isError,
    error,
  } = useCreateContest();
  const { data: categories } = useCategories();
  const [newContest, setNewContest] = useState({
    title: '',
    assets: [],
    isActive: false,
    note: '',
    date: null,
    durationFrom: now('Europe/Amsterdam'),
    durationTo: now('Europe/Amsterdam'),
    coordinates: {
      lat: '',
      lng: '',
    },
    categories: [],
  });

  console.log({ newContest });
  const onToggleIsActive = (isActive: boolean) => {
    setNewContest((state: any) => ({ ...state, isActive: isActive }));
  };
  const onChangeTitle = (title: string) => {
    setNewContest((state: any) => ({ ...state, title: title }));
  };
  const onChangeNote = (title: string) => {
    setNewContest((state: any) => ({ ...state, note: title }));
  };
  const onChangeDuration = ({ start, end }: RangeValue<DateValue>) => {
    setNewContest((state: any) => ({
      ...state,
      durationFrom: start,
      durationTo: end,
    }));
  };
  const onChangeDate = (date: ZonedDateTime) => {
    setNewContest((state: any) => ({ ...state, date: date }));
  };
  const onChangeLat = (lat: string) => {
    setNewContest((state: any) => ({
      ...state,
      coordinates: {
        ...state.coordinates,
        lat: lat,
      },
    }));
  };
  const onChangeLng = (lng: string) => {
    setNewContest((state: any) => ({
      ...state,
      coordinates: {
        ...state.coordinates,
        lng: lng,
      },
    }));
  };
  const onChangeAssets = (assets: any) => {
    setNewContest((state: any) => ({
      ...state,
      assets: [...(state?.assets?.length ? state.assets : []), assets],
    }));
  };
  const onSelectCategories = (categories: any) => {
    setNewContest((state: any) => ({
      ...state,
      categories: [...categories],
    }));
  };

  const { isOpen, onOpen, onOpenChange, onClose } = useDisclosure();
console.log({newContest})
  const onSubmit = useCallback(() => {
    const transformedMedia = newContest.assets.map((assets) =>
      transformMedia(assets)
    );
    const transformedDate = newContest?.date
      ? { date: transformDateToISO(newContest?.date) }
      : {};
    const transformedNewContest = {
      ...newContest,
      ...transformedDate,
      location: {
        coordinates: {
          lng: Number(newContest.coordinates.lng),
          lat: Number(newContest.coordinates.lat),
        },
      },
      duration: {
        from: transformDateToISO(newContest?.durationFrom),
        to: transformDateToISO(newContest?.durationTo),
      },
      assets: transformedMedia,
      categories: Array.from(newContest.categories),
    };

    mutateCreateContest(transformedNewContest, {
      onSuccess: (data) => {
        window.location.reload();
        onClose();
      },
      onError: (error) => console.log({ error }),
    });
  }, [mutateCreateContest, newContest, onClose]);

  return (
    <div className="">
      <Button variant="bordered" onPress={onOpen}>
        Create Contest
      </Button>
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        backdrop={'blur'}
        scrollBehavior={'outside'}
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex flex-col gap-1">
                New Contest
              </ModalHeader>
              <ModalBody>
                <Input
                  placeholder="Title"
                  size="lg"
                  onValueChange={onChangeTitle}
                ></Input>
                <Textarea
                  placeholder="Note"
                  size="lg"
                  height={200}
                  multiple
                  onValueChange={onChangeNote}
                  value={newContest.note}
                ></Textarea>
                <DatePicker
                  label="Date"
                  fullWidth
                  // hideTimeZone
                  granularity="second"
                  showMonthAndYearPickers
                  defaultValue={now('Europe/Amsterdam')}
                  onChange={onChangeDate}
                />
                <Input
                  placeholder="Latitude"
                  size="lg"
                  value={`${newContest.coordinates.lat}`}
                  onValueChange={onChangeLat}
                ></Input>
                <Input
                  placeholder="Longitude"
                  size="lg"
                  value={`${newContest.coordinates.lng}`}
                  onValueChange={onChangeLng}
                ></Input>
                <DateRangePicker
                  fullWidth
                  label="Contest duration"
                  onChange={onChangeDuration}
                />
                <Select
                  label="Select categories"
                  placeholder="Select categories"
                  selectionMode="multiple"
                  onChange={(event) => {
                    const selectedCategories = event.target.value.split(',');
                    onSelectCategories(selectedCategories);
                  }}
                  fullWidth
                >
                  {categories?.map((category: any) => (
                    <SelectItem key={category.id}>{category.title}</SelectItem>
                  ))}
                </Select>
                <UploadInput
                  presetVarants={CDY_PRESETS_VARIANTS.contest}
                  files={newContest.assets}
                  onChange={onChangeAssets}
                ></UploadInput>
                <Checkbox size="lg" onValueChange={onToggleIsActive}>
                  Push to the app
                </Checkbox>
              </ModalBody>
              <ModalFooter>
                {isError ? <h3>{error?.toString()}</h3> : null}
                <Button color="danger" variant="light" onPress={onClose}>
                  Close
                </Button>
                <Button
                  color="primary"
                  onPress={onSubmit}
                  isLoading={isLoading}
                >
                  Save
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </div>
  );
};

export default CreateContestModal;
