'use client';

import { api } from '@api/api-client';
import { useMutation, useQuery } from 'react-query';

export const useCreateContest = () => {
  return useMutation({
    mutationFn: async (newContest: any) => {
      const { data, status } = await api.post('/admin-contests', newContest);
      return data;
    },
  });
};

export const useContests = () => {
  return useQuery('contests', () => api.get('/admin-contests'), {
    select: (result) => result.data,
  });
};

export const useContest = (id: string) => {
  return useQuery({
    // queryKey: [`admin-contest-${id}`],
    queryFn: () => api.get(`/admin-contests/${id}`),
    select: (result) => result.data,
    // enabled: !!id, 
  });
};
