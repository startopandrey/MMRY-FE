'use client';

import { Button } from '@components/UI/button.component';
import UploadInput from '@components/shared/upload-input/upload-input.component';

import {
  Checkbox,
  DatePicker,
  DateRangePicker,
  Input,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  Select,
  SelectItem,
  Textarea,
  useDisclosure,
} from '@nextui-org/react';
import React, { useCallback, useState } from 'react';
import { ICategoryDB } from 'src/types/category.type';
import { now, getLocalTimeZone, ZonedDateTime } from '@internationalized/date';
import { CDY_PRESETS_VARIANTS } from 'src/utils/constants.utils';
import { useCategories } from 'src/features/Categories/services/index.service';
import { useCreateActivity } from '../services/index.service';
import { useQueryClient } from 'react-query';
import { transformMedia } from 'src/features/helper/index.helper';
import { useRouter } from 'next/navigation';
import { transformDateToISO } from '@helpers/date.helper';

const CreateActivityModal = () => {
  const router = useRouter();
  const queryClient = useQueryClient();
  const {
    mutate: mutateCreateActivity,
    isLoading,
    isError,
    error,
  } = useCreateActivity();
  const { data: categories } = useCategories();
  const [newActivity, setNewActivity] = useState<any>({
    title: '',
    assets: [],
    isActive: false,
    date: null,
    note: '',
    coordinates: {
      lat: '',
      lng: '',
    },
    categories: [],
  });
  console.log({ newActivity }, 3);
  const onToggleIsActive = (isActive: boolean) => {
    setNewActivity((state: any) => ({ ...state, isActive: isActive }));
  };
  const onChangeTitle = (title: string) => {
    setNewActivity((state: any) => ({ ...state, title: title }));
  };
  const onChangeNote = (title: string) => {
    setNewActivity((state: any) => ({ ...state, note: title }));
  };
  const onChangeDate = (date: ZonedDateTime) => {
    setNewActivity((state: any) => ({ ...state, date: date }));
  };
  const onChangeLat = (lat: string) => {
    setNewActivity((state: any) => ({
      ...state,
      coordinates: {
        ...state.coordinates,
        lat: lat,
      },
    }));
  };
  const onChangeLng = (lng: string) => {
    setNewActivity((state: any) => ({
      ...state,
      coordinates: {
        ...state.coordinates,
        lng: lng,
      },
    }));
  };
  const onChangeAssets = (assets: any) => {
    setNewActivity((state: any) => ({
      ...state,
      assets: [...(state?.assets?.length ? state.assets : []), assets],
    }));
  };
  const onSelectCategories = (categories: any) => {
    setNewActivity((state: any) => ({
      ...state,
      categories: [...categories],
    }));
  };

  const { isOpen, onOpen, onOpenChange, onClose } = useDisclosure();

  const onSubmit = useCallback(() => {
    const transformedMedia = newActivity.assets.map((assets: any) =>
      transformMedia(assets)
    );
    const transformedDate = newActivity?.date
    ? { date: transformDateToISO(newActivity?.date) }
    : {};
    const transformedNewActivity = {
      ...newActivity,
      ...transformedDate,
      location: {
        coordinates: {
          lng: Number(newActivity.coordinates.lng),
          lat: Number(newActivity.coordinates.lat),
        },
      },
      assets: transformedMedia,
      categories: Array.from(newActivity.categories),
    };

    mutateCreateActivity(transformedNewActivity, {
      onSuccess: (data) => {
        onClose();
        window.location.reload();
      },
      onError: (error) => console.log({ error }),
    });
  }, [mutateCreateActivity, newActivity, onClose]);

  return (
    <div className="">
      <Button variant="bordered" onPress={onOpen}>
        Create Activity
      </Button>
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        backdrop={'blur'}
        scrollBehavior={'outside'}
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex flex-col gap-1">
                New Activity
              </ModalHeader>
              <ModalBody>
                <Input
                  placeholder="Title"
                  size="lg"
                  value={newActivity.title}
                  onValueChange={onChangeTitle}
                ></Input>
                <Textarea
                  placeholder="Note"
                  size="lg"
                  height={200}
                  multiple
                  onValueChange={onChangeNote}
                  value={newActivity.note}
                ></Textarea>
                <DatePicker
                  label="Date"
                  fullWidth
                  // hideTimeZone
                  granularity="second"
                  showMonthAndYearPickers
                  defaultValue={now('Europe/Amsterdam')}
                  onChange={onChangeDate}
                />
                <Input
                  placeholder="Latitude"
                  size="lg"
                  value={`${newActivity.coordinates.lat}`}
                  onValueChange={onChangeLat}
                ></Input>
                <Input
                  placeholder="Longitude"
                  size="lg"
                  value={`${newActivity.coordinates.lng}`}
                  onValueChange={onChangeLng}
                ></Input>
                <Select
                  label="Select categories"
                  placeholder="Select categories"
                  selectionMode="multiple"
                  onChange={(event) => {
                    const selectedCategories = event.target.value.split(',');
                    onSelectCategories(selectedCategories);
                  }}
                  fullWidth
                >
                  {categories?.map((category: any) => (
                    <SelectItem key={category.id}>{category.title}</SelectItem>
                  ))}
                </Select>
                <Checkbox
                  isSelected={newActivity.isActive}
                  onValueChange={onToggleIsActive}
                  size="lg"
                >
                  Push to the app
                </Checkbox>
                <UploadInput
                  files={newActivity.assets}
                  onChange={onChangeAssets}
                  presetVarants={CDY_PRESETS_VARIANTS.activity}
                ></UploadInput>
              </ModalBody>
              <ModalFooter>
                {isError ? <h3>{error?.toString()}</h3> : null}
                <Button color="danger" variant="light" onPress={onClose}>
                  Close
                </Button>
                <Button
                  color="primary"
                  onPress={onSubmit}
                  isLoading={isLoading}
                >
                  Save
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </div>
  );
};

export default CreateActivityModal;
