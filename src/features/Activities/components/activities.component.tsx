import React from 'react';
import ActivityCardRow from './activity-card-row.component';

const Activities = ({data}: any) => {
  return (
    <div>
      {data?.map((activity: any) => (
        <ActivityCardRow
          key={activity.id}
          id={activity.id}
          title={activity.title}
        ></ActivityCardRow>
      ))}
    </div>
  );
};

export default Activities;
