import { Link } from '@nextui-org/react';
import React from 'react';

const ActivityCardRow = ({ title, id }: any) => {
  return (
    <div className='flex flex-col'>
      <Link isBlock showAnchorIcon isDisabled={true} color="primary" className='w-fit'>
      {title}
      </Link>
    </div>
  );
};

export default ActivityCardRow;
