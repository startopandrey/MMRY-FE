'use client';

import { api } from '@api/api-client';
import { useMutation, useQuery } from 'react-query';

export const useCreateActivity = () => {
  return useMutation({
    mutationFn: async (newActivity: any) => {
      const { data, status } = await api.post('/admin-activities', newActivity);
      return data;
    },
  });
};

export const useActivities = () => {
  return useQuery('categories', () => api.get('/admin-categories'), {
    select: (result) => result.data,
  });
};

export const useCategory = (id: string) => {
  return useQuery(`category-${id}`, () => api.get(`/admin-categories/${id}`), {
    select: (result) => result.data,
  });
};
