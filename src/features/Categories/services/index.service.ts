'use client';

import { api } from '@api/api-client';
import { useMutation, useQuery } from 'react-query';

export const useCreateCategory = () => {
  return useMutation({
    mutationFn: async (newCategory: any) => {
      console.log({ newCategory }, 43);
      const { data, status } = await api.post('/admin-categories', newCategory);
      return data;
    },
  });
};

export const useCategories = () => {
  return useQuery('categories', () => api.get('/admin-categories'), {
    select: (result) => result.data,
  });
};

export const useCategory = (id: string) => {
  return useQuery(`category-${id}`, () => api.get(`/admin-categories/${id}`), {
    select: (result) => result.data,
  });
};
