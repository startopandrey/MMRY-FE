'use client';

import { Button } from '@components/UI/button.component';
import UploadInput from '@components/shared/upload-input/upload-input.component';

import {
  Checkbox,
  DateRangePicker,
  Input,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  useDisclosure,
} from '@nextui-org/react';
import React, { useCallback, useState } from 'react';
import { MediaAsset } from 'src/types/memories.type';
import { CDY_PRESETS_VARIANTS } from 'src/utils/constants.utils';
import { useCreateCategory } from '../services/index.service';
import { transformMedia } from 'src/features/helper/index.helper';
import { useRouter } from 'next/navigation'
import { useQueryClient } from 'react-query';

interface INewCategory {
  title: string;
  assets: MediaAsset[];
  isPopular: boolean;
  isActive: boolean;
  isSeparate: boolean;
  color: string;
}
const CreateCategoryModal = () => {
  const router = useRouter();
  const queryClient = useQueryClient();
  const {
    mutate: mutateCreateCategory,
    isLoading,
    isError,
    error,
  } = useCreateCategory();
  const [newCategory, setNewCategory] = useState<INewCategory>({
    title: '',
    assets: [],
    isPopular: false,
    isActive: false,
    isSeparate: false,
    color: '',
  });
  const { isOpen, onOpen, onOpenChange, onClose } = useDisclosure();

  const onToggleIsActive = (isActive: boolean) => {
    setNewCategory((state) => ({ ...state, isActive: isActive }));
  };
  const onToggleIsSeparate = (isSeparate: boolean) => {
    setNewCategory((state) => ({ ...state, isSeparate: isSeparate }));
  };
  const onToggleIsPopular = (isActive: boolean) => {
    setNewCategory((state) => ({ ...state, isPopular: isActive }));
  };
  const onChangeTitle = (title: string) => {
    setNewCategory((state) => ({ ...state, title: title }));
  };
  const onChangeColor = (title: string) => {
    setNewCategory((state) => ({ ...state, color: title }));
  };
  const onChangeAssets = (media: any) => {
    setNewCategory((state) => ({
      ...state,
      assets: [...(state?.assets?.length ? state.assets : []), media],
    }));
  };
  const onSubmit = useCallback(() => {
    const transformedMedia = newCategory.assets.map((assets) =>
      transformMedia(assets)
    );
    const transformedNewCategory = { ...newCategory, assets: transformedMedia };
    mutateCreateCategory(transformedNewCategory, {
      onSuccess: (data) => {
        console.log({ data });
        onClose();
        queryClient.invalidateQueries('categories')
        router.replace(`/admin/marketing/categories/${data._id}/activities`);
      },
      onError: (error) => console.log({ error }),
    });
  }, [mutateCreateCategory, newCategory, onClose]);

  return (
    <div className="">
      <Button variant="bordered" onPress={onOpen}>
        Create category
      </Button>
      <Modal isOpen={isOpen} onOpenChange={onOpenChange} backdrop={'blur'} scrollBehavior={'outside'}>
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex flex-col gap-1">
                New Category
              </ModalHeader>
              <ModalBody>
                <Input
                  placeholder="Title"
                  size="lg"
                  onValueChange={onChangeTitle}
                ></Input>
                <Input
                  placeholder="HEX Color"
                  size="lg"
                  onValueChange={onChangeColor}
                ></Input>

                <UploadInput
                  presetVarants={CDY_PRESETS_VARIANTS.category}
                  files={newCategory.assets}
                  onChange={onChangeAssets}
                ></UploadInput>
                <Checkbox size="lg" onValueChange={onToggleIsPopular}>
                  Populair Category
                </Checkbox>
                <Checkbox size="lg" onValueChange={onToggleIsActive}>
                  Push to the app
                </Checkbox>
                <Checkbox size="lg" onValueChange={onToggleIsSeparate}>
                Horizontal Separated
                </Checkbox>
              </ModalBody>
              <ModalFooter>
                {isError ? <h3>{error?.toString()}</h3> : null}

                <Button color="danger" variant="light" onPress={onClose}>
                  Close
                </Button>
                <Button
                  color="primary"
                  onPress={onSubmit}
                  isLoading={isLoading}
                >
                  Save
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </div>
  );
};

export default CreateCategoryModal;
