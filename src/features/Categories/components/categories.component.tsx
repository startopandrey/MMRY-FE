'use client';

import React from 'react';
import CategoriesCardRow from './categories-card-row.component';
import { useCategories } from '../services/index.service';

const Categories = () => {
  const { data } = useCategories();
  return (
    <div>
      {data?.map((category: any) => (
        <CategoriesCardRow
          key={category._id}
          id={category._id}
          title={category.title}
        ></CategoriesCardRow>
      ))}
    </div>
  );
};

export default Categories;
