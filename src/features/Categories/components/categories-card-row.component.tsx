import { Link } from '@nextui-org/react';
import React from 'react';

const CategoriesCardRow = ({ title, id }: any) => {
  return (
    <div className='flex flex-col'>
      <Link isBlock showAnchorIcon href={`/admin/marketing/categories/${id}/activities`} color="primary" className='w-fit'>
      {title}
      </Link>
    </div>
  );
};

export default CategoriesCardRow;
