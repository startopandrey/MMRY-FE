const { nextui } = require('@nextui-org/react');
import plugin from 'tailwindcss/plugin';
const { pick, omit } = require('lodash');
const colors = require('tailwindcss/colors');
const defaultTheme = require('tailwindcss/defaultTheme');

/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: [
    './index.html',
    './src/**/*.{js,ts,jsx,tsx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/**/*.{js,ts,jsx,tsx,mdx}',
    './node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      screens: {
        xsm: '390px',
      },
      transitionProperty: {
        width: 'width',
      },
      container: {
        center: true,
        padding: '1rem',
        screens: {
          '2xl': '1400px',
        },
      },
      colors: {
        primary: {
          50: '#ebf6fc',
          100: '#c2e3f5',
          200: '#a5d6f0',
          300: '#7cc3e9',
          400: '#62b7e5',
          500: '#3ba5de',
          600: '#3696ca',
          700: '#2a759e',
          800: '#205b7a',
          900: '#19455d',
          DEFAULT: '#3ba5de',
        },
        secondary: '#f2f2f2',
        card: {
          50: '#f0f1ff',
          100: '#cfd3fe',
          200: '#b8befd',
          300: '#98a1fc',
          400: '#848efc',
          500: '#6572fb',
          600: '#5c68e4',
          700: '#4851b2',
          800: '#383f8a',
          900: '#2a3069',
          DEFAULT: '#6572fb',
        },
        shadow: {
          50: '#f2f2f2',
          100: '#d7d7d7',
          200: '#c3c3c3',
          300: '#a8a8a8',
          400: '#979797',
          500: '#7d7d7d',
          600: '#727272',
          700: '#595959',
          800: '#454545',
          900: '#353535',
          DEFAULT: '#595959',
        },
        danger: {
          50: '#fdeeec',
          100: '#f8c9c3',
          200: '#f4afa6',
          300: '#f08b7e',
          400: '#ed7565',
          500: '#e8523e',
          600: '#d34b38',
          700: '#a53a2c',
          800: '#802d22',
          900: '#61221a',
          DEFAULT: '#e8523e',
        },
      },
      fontFamily: {
        body: [
          'Nunito',
          'ui-sans-serif',
          'system-ui',
          '-apple-system',
          'system-ui',
          'Segoe UI',
          'Roboto',
          'Helvetica Neue',
          'Arial',
          'Noto Sans',
          'sans-serif',
          'Apple Color Emoji',
          'Segoe UI Emoji',
          'Segoe UI Symbol',
          'Noto Color Emoji',
        ],
        sans: [
          'Nunito',
          'ui-sans-serif',
          'system-ui',
          '-apple-system',
          'system-ui',
          'Segoe UI',
          'Roboto',
          'Helvetica Neue',
          'Arial',
          'Noto Sans',
          'sans-serif',
          'Apple Color Emoji',
          'Segoe UI Emoji',
          'Segoe UI Symbol',
          'Noto Color Emoji',
        ],
      },
      borderWidth: {
        DEFAULT: '1px',
        0: '0',
        2: '2px',
        3: '3px',
        4: '4px',
        6: '6px',
        8: '8px',
      },
      minHeight: {
        ...defaultTheme.height,
      },
      minWidth: {
        ...defaultTheme.width,
      },
    },
  },
  darkMode: ['class'],
  plugins: [
    nextui(),
    plugin(function ({ addBase, addComponents, addUtilities }) {
      addBase({});
      addComponents({
        '.container': {
          '@apply max-w-[77.5rem] px-5 md:px-10 lg:px-16 xl:max-w-[87.5rem]':
            {},
        },
        '.h1': {
          '@apply font-semibold text-[2rem] leading-[3.25rem] md:text-[2.75rem] md:leading-[3.75rem] lg:text-[3.25rem] lg:leading-[4.0625rem] xl:text-[3.75rem] xl:leading-[4.5rem]':
            {},
        },
        '.h2': {
          '@apply font-semibold text-[1.75rem] leading-[2.5rem] md:text-[2rem] md:leading-[2.5rem] lg:text-[2.5rem] lg:leading-[3.5rem] xl:text-[3rem] xl:leading-tight':
            {},
        },
        '.h3': {
          '@apply font-semibold text-[2rem] leading-normal md:text-[2.5rem]':
            {},
        },
        '.h4': {
          '@apply font-semibold  md:text-[1.6rem] lg:text-[2rem] leading-normal':
            {},
        },
        '.h5': {
          '@apply font-semibold text-2xl leading-normal': {},
        },
        '.h6': {
          '@apply font-semibold text-lg leading-8': {},
        },
        '.body-1': {
          '@apply text-[0.875rem] leading-[1.5rem] md:text-[1rem] md:leading-[1.75rem] lg:text-[1.25rem] lg:leading-8':
            {},
        },
        '.body-2': {
          '@apply text-[0.875rem] leading-6 md:text-base': {},
        },
        '.caption': {
          '@apply text-sm': {},
        },
        '.tagline': {
          '@apply text-sm uppercase': {},
        },
        '.quote': {
          '@apply text-lg leading-normal': {},
        },
        '.button': {
          '@apply text-sm font-bold uppercase tracking-wider': {},
        },
      });
      addUtilities({
        '.tap-highlight-color': {
          '-webkit-tap-highlight-color': 'rgba(0, 0, 0, 0)',
        },
      });
    }),
  ],
  future: {
    hoverOnlyWhenSupported: true,
  },
};
